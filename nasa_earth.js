var image_cell_template = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img></div><div class='image-caption'></div><div class='image-coords'></div></div>";
var date_tmpl = "begin=YEAR-MONTH-DAY";
var myApiKey = "api_key=mWopomNzzDfzsY5Z60aSZHag60HzaxryP4vNTRs4";

var example_image_url = "https://api.nasa.gov/EPIC/archive/natural/2017/08/21/png/epic_1b_20170821151450.png?api_key=mWopomNzzDfzsY5Z60aSZHag60HzaxryP4vNTRs4";
var epic_natural_archive_base = "https://api.nasa.gov/EPIC/archive/natural/";
var api_url_query_base = "https://epic.gsfc.nasa.gov/api/natural/date/";

// ==========================================================
// START JS: synchronize the javascript to the DOM loading
// ==========================================================
$(document).ready(function() {

  // ========================================================
  // SECTION 1:  process on click events
  // ========================================================
  $('#get-images-btn').on('click', api_search);

  // process the "future" img element dynamically generated
  $("div").on("click", "img", function(){
    console.log(this.src);
    // call render_highres() and display the high resolution
  });

  // ========================================================
  // TASK 1:  build the search AJAX call on NASA EPIC
  // ========================================================
  // Do the actual search in this function
  function api_search(e) {

    // get the value of the input search text box => date
    var date = $('#search_date').val();

    // build an info object to hold the search term and API key
    var info = {};
    var date_array = date.split('-');
    info.year = date_array[0];
    info.month = date_array[1];
    info.day = date_array[2];
    info.api_key = "mWopomNzzDfzsY5Z60aSZHag60HzaxryP4vNTRs4";

    // build the search url and sling it into the URL request HTML element
    var search_url = "https://epic.gsfc.nasa.gov/api/natural/date/" + info.year + "-" + info.month + "-" + info.day + "?api_key=" + info.api_key;
    console.log(search_url);
    // sling it!

    // make the jQuery AJAX call!
    $.ajax({
      url: search_url,
      success: function(data) {
        render_images(data,info);
      },
      cache: false
    });
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================

  // ========================================================
  // TASK 2: perform all image grid rendering operations
  // ========================================================
  function render_images(data,info) {
    // get NASA earth data from search results data
    // console.log(data.length);
    var images = [];
    for (var i = 0; i < data.length; i++) {
      // build an array of objects that captures all of the key image data

      // => image url
      var img_url = data[i]['image'];
    
      // => centroid coordinates to be displayed in the caption area
      var lat = data[i]['centroid_coordinates']['lat'];
      var lon = data[i]['centroid_coordinates']['lon'];

      // => image date to be displayed in the caption area (under thumbnail)
      var img_data = data[i]['date'];

      // push to list
      var capture = {}
      capture.url = img_url;
      capture.lat = lat;
      capture.lon = lon;
      capture.date = img_data;
      images.push(capture);
    }

    // select the image grid and clear out the previous render (if any)
    var earth_dom = $('#image-grid');
    earth_dom.empty();

    var current_row;

    //render all images in an iterative loop here!
    for(var i = 0; i < images.length; i++) {
      var img_url = "https://api.nasa.gov/EPIC/archive/natural/" + info.year + "/" + info.month + "/" + info.day + 
        "/png/" + images[i].url + ".png?api_key=" + info.api_key;

      var insert_row = `<div class='col-sm-3 image-cell'><div class='nasa-image'><img src="${img_url}"></div>\
      <div class='image-caption'>${images[i].date}</div>\
      <div class='image-coords'>lat:${images[i].lat} lon:${images[i].lon}</div></div>`;

      if((i % 4) === 0) {  
        current_row = '#row' + i;
          var insert_row = '<div class="row" id="row' + i + '">' + insert_row;
          $(earth_dom).append(insert_row);
          
      }
      else {
          $(current_row).append(insert_row);
      }
      
    }
    
  }

  // ========================================================
  // TASK 3: perform single high resolution rendering
  // ========================================================
  // function to render the high resolution image
  function render_highres(src_url) {
    // use jQuery to select and maniupate the portion of the DOM => #image-grid
    //  to insert your high resolution image
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
});
